package tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static pages.StaticMethods.*;

import org.junit.*;

import pages.*;

public class Homework5Test {

    @Before
    public void setUp() {
        Pages.setBaseUrl("http://mkalmo-hw5.appspot.com/");
        Pages.setSearchPageUrl("search");
        Pages.setAddPageUrl("addForm");
    }

    @Test
    public void addPageHasCustomerTypeSelect_2() {
        assertTrue(goToAddPage().isElementPresent("customerTypeSelect"));
    }

    @Test
    public void viewHasNoAddButton_1() {
        String code = randomString();
        ViewPage viewPage = goToAddPage().insert(code).view(code);

        assertFalse(viewPage.isElementPresent("addButton"));
    }

    @Test
    public void viewPageHasDisabledControls_2() {
        String code = randomString();
        ViewPage viewPage = goToAddPage().insert(code).view(code);

        assertTrue(viewPage.hasAttribute("firstNameBox", "disabled"));
        assertTrue(viewPage.hasAttribute("surnameBox", "disabled"));
        assertTrue(viewPage.hasAttribute("codeBox", "disabled"));
        assertTrue(viewPage.hasAttribute("customerTypeSelect", "disabled"));
    }

    @Test
    public void viewPagesBackLinkTakesToSearchPage_2() {
        String code = randomString();
        ViewPage viewPage = goToAddPage().insert(code).view(code);

        SearchPage searchPage = viewPage.clickBack();

        assertThat(searchPage.getPagePartFromUrl(), is(Pages.getSearchPageUrl()));
    }

    @Test
    public void viewShowsCustomerInfoOnTextFields_2() {
        String firstName = randomString();
        String surname = randomString();
        String code = randomString();

        SearchPage searchPage = goToAddPage().insert(
                customer().withFirstName(firstName)
                          .withSurname(surname)
                          .withCode(code));

        ViewPage viewPage = searchPage.view(code);

        assertThat(viewPage.getAttribute("firstNameBox", "value"), is(firstName));
        assertThat(viewPage.getAttribute("surnameBox", "value"), is(surname));
        assertThat(viewPage.getAttribute("codeBox", "value"), is(code));
    }

    @Test
    public void correctCustomerTypeIsSelected_2() {
        String type = "customerType.corporate";
        String code = randomString();

        SearchPage searchPage = goToAddPage().insert(
                customer().withCode(code)
                          .withType(type));

        ViewPage viewPage = searchPage.view(code);

        assertThat(viewPage.getSelectedType(), is(type));
    }

}
