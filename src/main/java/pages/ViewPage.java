package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ViewPage extends Pages {

    public ViewPage(WebDriver driver) {
        this.driver = driver;
    }

    public SearchPage insert(String name, String code) {
        element("nameBox").sendKeys(name);
        element("codeBox").sendKeys(code);
        element("addButton").click();
        return Pages.createSearchPage(driver);
    }

    public SearchPage clickBack() {
        element("backLink").click();
        return Pages.createSearchPage(driver);
    }

    public String getSelectedType() {
        Select select = new Select(element("customerTypeSelect"));
        List<WebElement> options = select.getAllSelectedOptions();
        if (options.size() != 1) {
            throw new IllegalStateException("more than one option selected");
        }
        return options.get(0).getAttribute("value");
    }

    public List<String> getPhoneValues() {
        List<String> retval = new ArrayList<String>();

        for (WebElement each : findElementsMatching("phones{0}.value")) {
            retval.add(each.getAttribute("value"));
        }

        return retval;
    }

    public List<String> getPhoneTypes() {
        List<String> retval = new ArrayList<String>();

        for (WebElement each : findElementsMatching("phones{0}.type")) {
            Select select = new Select(each);
            String type = select.getAllSelectedOptions().get(0)
                    .getAttribute("value");

            retval.add(type);
        }

        return retval;
    }

}
