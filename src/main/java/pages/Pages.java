package pages;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class Pages {

    private static String baseUrl;
    private static String searchPageUrl = "Search";
    private static String addPageUrl = "Add";

    protected WebDriver driver;

    private static long seed = System.nanoTime();

    public static String randomString() {
        return String.valueOf(seed++);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public static WebDriver getNewDriver() {
        return new HtmlUnitDriver();
    }

    public boolean isElementPresent(String id) {
        return driver.findElements(By.id(id)).size() > 0;
    }

    public List<WebElement> findElementsMatching(String pattern) {
        List<WebElement> retval = new ArrayList<WebElement>();

        for (int i = 0;;i++) {
            String id = MessageFormat.format(pattern, i);
            if (isElementPresent(id)) {
                retval.add(element(id));
            } else {
                break;
            }
        }

        return retval;
    }

    public WebElement element(String id) {
        return driver.findElement(By.id(id));
    }

    public String getAttribute(String id, String attributeName) {
        return driver.findElement(By.id(id)).getAttribute(attributeName);
    }

    public boolean hasAttribute(String id, String attributeName) {
        return getAttribute(id, attributeName) != null;
    }

    public String getPageSource() {
        return driver.getPageSource();
    }

    protected void goTo(String pageName) {
        String url = getUrl(pageName);

        driver.get(getUrl(pageName));

        String source = driver.getPageSource();
        if (source.contains("Status 404")) {
            throw new IllegalStateException("404 " + url);
        }
        if (source.contains("Status 500")) {
            throw new IllegalStateException("500 " + url);
        }

    }

    protected String getUrl(String page) {

        String baseUrl = System.getProperty("batchBaseUrl");
        if (baseUrl == null) {
            baseUrl = Pages.baseUrl;
        }

        return baseUrl + page;
    }

    public static void setBaseUrl(String url) {
        baseUrl = url;
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public String getPagePartFromUrl() {
        String[] splited = removeSessionId(getCurrentUrl()).split("/");
        return splited.length > 0 ? splited[splited.length - 1] : "";
    }

    protected String removeSessionId(String url) {
        return url.contains(";")
                ? url.substring(0, url.indexOf(";"))
                : url;
    }

    public String getSource() {
        return driver.getPageSource();
    }

    protected List<String> getIdsStartingWith(String prefix) {
        List<String> retval = new ArrayList<String>();

        List<WebElement> elements = driver.
                findElements(By.cssSelector(
                        MessageFormat.format("[id^=''{0}'']", prefix)));

        for (WebElement each : elements) {
            retval.add(each.getAttribute("id"));
        }

        return retval;
    }

    public static SearchPage createSearchPage(WebDriver driver) {
        return new SearchPage(searchPageUrl, driver);
    }

    public static AddPage createAddPage(WebDriver driver) {
        return new AddPage(addPageUrl, driver);
    }

    public static void setSearchPageUrl(String url) {
        searchPageUrl = url;
    }

    public static void setAddPageUrl(String url) {
        addPageUrl = url;
    }

    public static String getSearchPageUrl() {
        return searchPageUrl;
    }

    public boolean isSearchPage() {
        return isElementPresent("listTable");
    }

    public boolean isAddPage() {
        return isElementPresent("formTable");
    }

    public boolean isLoginPage() {
        return isElementPresent("loginPage");
    }

    public boolean hasMessageBlock() {
        return isElementPresent("messageBlock");
    }
}
